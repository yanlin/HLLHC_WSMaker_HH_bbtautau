# Create the workspace and run the limits

## Edit scripts/Analysis_HH.py

Set channel, for example:
```bash
chan = "hadhad"
```
Set input version, for example:
```bash
InputVersion = "HH190401"
```

Choose other options, for example:
```bash
dozcr = True    # to use Zmumu+hf CR
dobdt = True    # to use BDT score as the final discriminant, see: scripts/AnalysisMgr_HH.py
doLTT = True    # to inlude LTT for lephad or the combination

masses = [...]  # mass grid for resonant
lambdas = []    # leave empty if not interested in running the varied lambda
```
Relevant lines for creating the config files (for example this will create the workspace for the SMRW signal, stat-only):
```bash
configfiles_stat += add_config("0", "StatOnly", signal="SMRW", chan=chan, BDT = dobdt, zcr = dozcr, plots = doplots, LTT = doLTT)
```
Run command (``outversion`` will be used together with the input version to create the name of the workspace and other results):
```bash
python scripts/Analysis_HH.py <outversion>
```
The config file, logs, workspace, plots and root_files (limits) will be saved in the ``output/...`` directory

## edit/look at: scripts/AnalysisMgr_HH.py

Define regions, final discriminant, variables to plot, etc.

## edit/look at: scripts/limit_NEW.py

Setup channel, InputVersion and add a line to extract the limit, an example for the SMRW signal:
```bash
outver = sys.argv[1] if len(sys.argv) > 1 else ""
if chan == "hadhad":
        non_res(inver + "." + outver + "_HH_13TeV_" + outver + "_StatOnly_hadhad_SMRW_BDT_0", chan="hadhad")
        #non_res(inver + "." + outver + "_HH_13TeV_" + outver + "_Systs_hadhad_SMRW_BDT_0", chan="hadhad")
sys.exit()
```
See other examples in the script (how to plot limits as a function of resonant mass, etc.)

Run to extract the limits:
```bash
python scripts/limit_NEW.py <outversion>
```

## Samples, binning, systematics

src/binning_hh.cpp

src/samplesbuilder_hh.cpp

src/systematiclistsbuilder_hh.cpp


##Example commands to make pulls, rankings, postfit plots and tables are in:
scripts/run.hadhad.sh 

