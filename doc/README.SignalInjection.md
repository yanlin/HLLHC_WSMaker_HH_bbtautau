For running signal injection tests:

1) Set doInjection to True here: https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/bbtautau/WSMaker/-/blob/master/scripts/getLimit.py#L42

2) Set the signal mu values for the injection 
- for resonant here: https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/bbtautau/WSMaker_HH_bbtautau/-/blob/master/src/systematiclistsbuilder_hh.cpp#L52
- for non-resonant here: https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/bbtautau/WSMaker_HH_bbtautau/-/blob/master/src/systematiclistsbuilder_hh.cpp#L60

3) Set the signal mass for injection here: https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/bbtautau/WSMaker_HH_bbtautau/-/blob/master/scripts/Analysis_HH.py#L56

4) Run as usual

5) To get the limits after injection run scripts/limit_New.py setting minjection to the injected mass in limit_New.py 

6) To get the mu hat after injection run scripts/