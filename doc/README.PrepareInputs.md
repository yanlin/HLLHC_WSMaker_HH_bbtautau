# Prepare the inputs

Merge all the root files produced by MIA/CxAODReader, or some other framework. The histogram names should follow:

```bash
<sample>_<N>tag2pjet_0ptv_<optional region name>_<variable name>
```
An example:
```bash
Fake_2tag2pjet_0ptv_SR_mHH
```

Relevant histograms need to be in the top directory. If they are placed in some directory, the following python script can be used to promote them to the top directory:
<https://gitlab.cern.ch/MIA/MIA/blob/master/python/selectAndSlim.py>

Usage:
```bash
python selectAndSlim.py [input file name] [output file name] [list of hist names/dirs to keep] [list of dir names to promote to top-level] [skip SS ?] 
python selectAndSlim.py InputFile.root OutputFile.root BDTVarsPreselection BDTVarsPreselection 1
```

# Split the inputs

Create a config file in the ``inputConfigs/`` directory, named ``<InputVersion>.txt``, e.g. HH190401.txt. The config should contain the name of the channel and the name of the ws input (that has the relevant histograms in the top directory). The name of the ws input root file has to contain ``*_13TeV_*``. Only the name of the file should be given, the path to it is provided in the next step.
The name of the channel in the config file should be TauHH for hadhad, TauLH for lephad SLT and TauLH_LTT for lephad LTT

An example of the content of the config file:
```bash
TauHH Name_of_the_input_13TeV_190401.root
```

Command to split the inputs:
```bash
SplitInputs -v <InputVersion, without “.txt”> -r Run2 -inDir <path to the ws input>
```

Split inputs will be created in the ``inputs/<InputVersion>/`` directory



There will be one config file and then one split inputs directory for each input file produced with MIA/CxAODReader, so one for each channel:
- LepHad SLT
- LepHad LTT
- HadHad
- ZCR

If you wish to run the limits including more than one channel you need to create a new directory in the inputs directory and copy the split files of all the channels you want to combine in the same directory.

# Post-processing of the inputs

We might want to rename/remove some processes or systematics before the fit. This can be done directly on the histogram root files in `inputs/`. This is useful for the combined fit where inputs are produced in different channels but in some cases one wants to correlated the nuisance parameters.

Command to do the post-processing:
```bash
python scripts/inputProcessing.py inputs/<InputVersion>/<Input1> inputs/<InputVersion>/<Input2> ... inputs/<InputVersion>/<InputN> inputs/<NewInputVersion>/ -c <Channel> -s <Signal>

# Example
python scripts/inputProcessing.py inputs/hadhad/13TeV_TauHH_2tag2pjet_0ptv_LL_OS_SMBDT.root inputs/hadhad_v1/13TeV_TauHH_2tag2pjet_0ptv_LL_OS_PNN*.root inputs/comb/ -c hadhad

python scripts/inputProcessing.py inputs/lephad_SLT/13TeV_TauLH_2tag2pjet_0ptv_SM_NN.root inputs/lephad_SLT/13TeV_TauLH_2tag2pjet_0ptv_2HDM_PNN_*.root inputs/lephad_LTT/13TeV_TauLH_LTT_2tag2pjet_0ptv_SM_NN.root inputs/lephad_LTT/13TeV_TauLH_LTT_2tag2pjet_0ptv_2HDM_PNN_*.root inputs/comb/ -c lephad

python scripts/inputProcessing.py inputs/zcr/13TeV_TwoLepton_2tag2pjet_0ptv_ZllbbCR_mLL.root inputs/comb/ -c zcr

# To specify the signal for non-MVAscore inputs
python scripts/inputProcessing.py inputs/hadhad/13TeV_TauHH_2tag2pjet_0ptv_LL_OS_mHH.root inputs/comb/ -c hadhad -s SMBDT

```




