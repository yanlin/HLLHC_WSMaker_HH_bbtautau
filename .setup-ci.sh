# Analysis Type Enum
export ANALYSISTYPE="HHbbtautau"
echo "Setting ANALYSISTYPE to : $ANALYSISTYPE"

# Analysis directory is here
# Work directory is the Core
export ANALYSISDIR=${PWD}
export WORKDIR=${ANALYSISDIR}/WSMakerCore
export BUILDDIR=${ANALYSISDIR}/build

export IS_BLINDED=0
if [ $IS_BLINDED = "1" ];
then
  echo "Blinding the analysis."
else
  echo "Beware the analysis is not blinded"
fi

export NCORE=4

# increase stack size - needed for large workspaces
ulimit -S -s 15360

# Paths
export LD_LIBRARY_PATH=${ROOTSYS}/lib:${LD_LIBRARY_PATH}
export PATH=${ROOTSYS}/bin:${PATH}
export PYTHONPATH=${ROOTSYS}/lib:${PYTHONDIR}/lib:${PYTHONPATH}

export LD_LIBRARY_PATH=${BUILDDIR}:${BUILDDIR}/WSMakerCore:${WORKDIR}:${LD_LIBRARY_PATH}
export PATH=${BUILDDIR}:${WORKDIR}/scripts:${ANALYSISDIR}/scripts:${PATH}
export PYTHONPATH=${ANALYSISDIR}/scripts:${WORKDIR}/scripts:${PYTHONPATH}

mkdir -vp ${BUILDDIR}
mkdir -vp ${ANALYSISDIR}/configs
mkdir -vp ${ANALYSISDIR}/output
