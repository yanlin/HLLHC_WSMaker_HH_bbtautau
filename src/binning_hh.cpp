#include "binning_hh.hpp"

#include <iostream>
#include <cmath>
#include <map>
#include <memory>
#include <sstream>

#include <TH1.h>
#include <TString.h>

#include "WSMaker/binning.hpp"
#include "WSMaker/configuration.hpp"
#include "WSMaker/category.hpp"
#include "WSMaker/properties.hpp"

#include "TransformTool/HistoTransform.h"

class Sample;

std::vector<int> getBinningFromEnv(const char *);


BinningTool_HH::BinningTool_HH(const Configuration& conf) :
  BinningTool(conf)
{
}

void BinningTool_HH::createInstance(const Configuration& conf) {
  if( !the_instance ) {
    std::cout << "INFO:    BinningTool_HH::createInstance() BinningTool pointer is NULL." << std::endl;
    std::cout << "         Will instanciate the BinningTool service first." << std::endl;
    the_instance.reset(new BinningTool_HH(conf));
  }
  else {
    std::cout << "WARNING: BinningTool_HH::createInstance() BinningTool pointer already exists." << std::endl;
    std::cout << "         Don't do anything !" << std::endl;
  }
}


std::vector<int> BinningTool_HH::getCategoryBinning(const Category& c) {

  // For some stupid reason the bins need to go backwards!

  int forceRebin = m_config.getValue("ForceBinning", 0);
  bool postfit = m_config.getValue("Postfit", 0);

  if(forceRebin > 0) {
    return {forceRebin};
  }

  if (m_config.getValue("OneBin", false)) {
    return oneBin(c);
  }

  if (c[Property::nTag] == 0) {
    return oneBin(c);
  }

//   if (c(Property::dist) == "mMMC") {
//     if (postfit) 
//       return {4};    
//     else
//       return {600, 254, 216, 190, 173, 158, 144, 129, 117, 107, 96, 84, 71}; // 8%
// 
//   }

//   if (c(Property::dist) == "TauPt") {
//     return {200, 144, 69, 51, 41, 32, 27, 23, 20}; // 5%
//   }

  if (c(Property::dist).Contains("MLQ")) {
    return {2000, 178, 114, 98, 88, 79, 70, 61, 52, 43, 34, 25, 16, 7};
  }

  if (c(Property::dist) == "sT") {
    return {2000, 196, 121, 107, 97, 91, 85, 79, 73, 67, 61, 55, 49, 43, 37, 31, 25};

  }

  if (c(Property::dist) == "mLL") {
    //return oneBin(c);
    return {1};
  }


  //std::vector<int> res{20};
  //std::vector<int> res{220, 246, 272, 298, 324, 350, 376, 402, 454, 550, 1200};

  //std::vector<int> res{0, 257, 260, 270, 280, 290, 300, 310, 320, 400, 450, 2000};

  std::vector<int> res;

  TString massPoint = m_config.getValue("MassPoint", "1000");
  TString type = m_config.getValue("Type", "RSG");

  if (c(Property::spec) == "TauHH") {
    // Can pass bins as environment variable to force a fixed binning
    // for validation plots
    if (std::getenv("WSMAKER_TAUHH_FORCEBINS")) {
      return getBinningFromEnv("WSMAKER_TAUHH_FORCEBINS");
    }

    if (c(Property::dist).EqualTo("mHH")) {
      res = {15};
    } else if (c(Property::dist).EqualTo("mMMC")) {
      res = {8};
    } else if (c(Property::dist).EqualTo("METCent")) {
      res = {1};
    } else if (c(Property::dist).EqualTo("mBB")) {
      res = {8};
    } else if (c(Property::dist).EqualTo("dRBB")) {
      res = {5};
    } else if (c(Property::dist).EqualTo("dRTauTau")) {
      res = {5};
    } else if (c(Property::dist).EqualTo("Tau0Pt")) {
      res = {8};  
    } else if (c(Property::dist).EqualTo("Tau1Pt")){
      res = {8};
    } else if (c(Property::dist).EqualTo("Tau0Eta")){
      res = {1};
    } else if (c(Property::dist).EqualTo("Tau1Eta")){
      res = {1};
    } else if (c(Property::dist).EqualTo("Jet0Pt")) {
      res = {8};  
    } else if (c(Property::dist).EqualTo("Jet1Pt")){
      res = {8};
    } else if (c(Property::dist).EqualTo("Jet0Eta")){
      res = {1};
    } else if (c(Property::dist).EqualTo("Jet1Eta")){
      res = {1};
    } else if (c(Property::dist).EqualTo("LeadJetPt")) {
      res = {8};  
    } else if (c(Property::dist).EqualTo("SubleadJetPt")){
      res = {8};
    } else if (c(Property::dist).EqualTo("LeadJetEta")){
      res = {1};
    } else if (c(Property::dist).EqualTo("SubleadJetEta")){
      res = {1};
    } else {
      TH1* sig = c.getSigHist();
      TH1* bkg = c.getBkgHist(); 
      m_htrafo->trafoSixY = 10;
      m_htrafo->trafoSixZ = 5;

      m_htrafo->trafo14MinBkg=5;

      std::vector<int> bins;
      if (type.Contains("SM")) {
	bins = m_htrafo->getRebinBins(bkg, sig, 14, 0.50);
	//bins = m_htrafo->getRebinBins(bkg, sig, 6);//test of trafo6
      } else {
	bins = m_htrafo->getRebinBins(bkg, sig, 14, 0.50);
	//bins = m_htrafo->getRebinBins(bkg, sig, 6);//test of trafo6
      }
      
      delete sig;
      delete bkg;

      for (std::size_t i=0; i<bins.size(); i++){
	std::cout<<"Bin "<<i<<" "<<bins[i]<<std::endl;
      }

      return bins;
    }
  } else {
    // Can pass bins as environment variable to force a fixed binning
    // for validation plots
    if (c[Property::LTT] && std::getenv("WSMAKER_TAULH_LTT_FORCEBINS")) {
      return getBinningFromEnv("WSMAKER_TAULH_LTT_FORCEBINS");
    }

    if (!c[Property::LTT] && std::getenv("WSMAKER_TAULH_SLT_FORCEBINS")) {
      return getBinningFromEnv("WSMAKER_TAULH_SLT_FORCEBINS");
    }

    if (c(Property::dist).EqualTo("mMMC")) {
      res = {8};
    } else if (c(Property::dist).EqualTo("METCent")) {
      res = {2};
    } else if (c(Property::dist).EqualTo("dPhiLep0MET")) {
      res = {4};
    } else if (c(Property::dist).EqualTo("MET")) {
      res = {4};
    } else if (c(Property::dist).EqualTo("dPhiHBB")) {
      res = {4};
    } else if (c(Property::dist).EqualTo("MtW")) {
      res = {2};
    } else if (c(Property::dist).EqualTo("mbb")) {
      res = {1};
    } else if (c(Property::dist).EqualTo("pTBB")) {
      res = {2};
    } else if (c(Property::dist).EqualTo("pTB2")) {
      res = {1};
    } else if (c(Property::dist).EqualTo("dPtLepTau")) {
      res = {2};
    } else if (c(Property::dist).EqualTo("dRbb")) {
      res = {5};
    } else if (c(Property::dist).EqualTo("DRTauTau")) {
      res = {5};  
    } else if (c(Property::dist).EqualTo("Mhh")) {
      res = {10};
    } else if (c(Property::dist).EqualTo("Ht")) {
      res = {5};  
    } else if (c(Property::dist).EqualTo("Tau0Pt")) {
      res = {5};  
    } else if (c(Property::dist).EqualTo("Tau1Pt")) {
      res = {5};  
    } else if (c(Property::dist).EqualTo("dPhiHttMET")) {
      res = {2};
    }
    //else if (type.Contains("SM") && c[Property::LTT]){ 
    //  res = {1002, 653};
    //}
    //else if (type.Contains("SM")){
    //  res = {1002, 771};  
    // }
    else {
      // BDT
      TH1* sig = c.getSigHist();
      TH1* bkg = c.getBkgHist(); 
      m_htrafo->trafoSixY = 10;
      m_htrafo->trafoSixZ = 5;

      m_htrafo->trafo14MinBkg=5;//5 min events also for Lephad

      //std::vector<int> bins = m_htrafo->getRebinBins(bkg, sig, 13, 0.08);
      //std::vector<int> bins = m_htrafo->getRebinBins(bkg, sig, 6, 0);
      std::vector<int> bins;
      if (type.Contains("SM") || c[Property::LTT]) {
	//bins = m_htrafo->getRebinBins(bkg, sig, 14, 0.40);
	bins = m_htrafo->getRebinBins(bkg, sig, 6, 0.40);//LepHad uses trafo6
      } else {
	//bins = m_htrafo->getRebinBins(bkg, sig, 14, 0.20);
	bins = m_htrafo->getRebinBins(bkg, sig, 6, 0.20);//LepHad uses trafo6
      }

      delete sig;
      delete bkg;

      for (std::size_t i=0; i<bins.size(); i++){
	std::cout<<"Bin "<<i<<" "<<bins[i]<<std::endl;
      }

      return bins;
      //return {10};
    }
  } 

  return res;
}

void BinningTool_HH::changeRange(TH1* h, const Category& c) {
  const TString dist = c(Property::dist);
  bool postfit = m_config.getValue("Postfit", 0);

  // special case: norm-only workspaces
  if (m_config.getValue("OneBin", false)) {
    return;
  }

  const auto isLepHad = c(Property::spec) == "TauLH";
  const auto isHadHad = c(Property::spec) == "TauHH";
  const auto isZCR = c(Property::nTag) == 2 && c(Property::nLep) == 2;

  if (isZCR && dist == "mLL") {
    changeRangeImpl(h, 75., 110., false);
  }

  return;
}


void BinningTool_HH::changeRangeAfter(TH1* h, const Category& c) {
  TString dist = c(Property::dist);



  // special case: norm-only workspaces
  if( m_config.getValue("OneBin", false)) {
    return;
  }




//   if(dist == "BDT") {
//     changeRangeImpl(h, 0, 1., false);
//   }

  return;
}


std::vector<int> getBinningFromEnv(const char *var) {
  const std::string env_binning = std::getenv(var);

  std::cout << "INFO:    Found environment variable " << var << std::endl;
  std::cout << "INFO:    Forcing binning to be: " << env_binning << std::endl;

  std::istringstream ss(env_binning);
  std::vector<int> forced_bins;

  std::string item;
  std::size_t cnt = 0;
  while (ss >> item) {
    std::cout << "Bin " << cnt++ << " " << item << std::endl;

    try {
      forced_bins.push_back(std::stoi(item));
    } catch (const std::exception &e) {
      std::cout << "WARNING: Cannot read binning from environment variable." << std::endl;
      throw;
    }
  }

  return forced_bins;
}
