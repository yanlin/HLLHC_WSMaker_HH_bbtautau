ARG AMG_TAG=${AMG_TAG}
FROM atlasamglab/stats-base:${AMG_TAG}

ARG WS_DIR=/${CI_PROJECT_NAME}

ADD . ${WS_DIR}
WORKDIR ${WS_DIR}

SHELL ["/bin/bash", "-c"]
ARG GITLAB_CI=${GITLAB_CI}

RUN pwd && source .setup-ci.sh && \
    cd build && cmake ../ && make -j && \
    echo -e "Instructions can be found in README.md"
