import ROOT as R

def plotRSGvs2HDM(f, v, masses, r=1):
    for m in masses:
        if "dr" in v.lower() and m <= 300:
            r = 2
        
        print ">>> {} @ {} ... ".format(v, m)
        hr = f.Get("Ghhbbtautau{}c10".format(m))
        hr2 = f.Get("Ghhbbtautau{}c20".format(m))        
        ht = f.Get("Hhhbbtautau{}".format(m))

        if hr: hr.SetLineColor(1)
        hr2.SetLineColor(2)        
        ht.SetLineColor(3)
        if hr: hr.Rebin(r)
        hr2.Rebin(r)        
        ht.Rebin(r)        

        ht.SetTitle("Compare RSG and 2HDM signals for {};{};Events".format(m, v))

        c = R.TCanvas()

        ht.DrawNormalized("ehist")        
        hr2.DrawNormalized("ehistsame")        
        if hr: hr.DrawNormalized("ehistsame")
        R.gPad.SetLogy(False)
        c.Print("RSGvs2HDM_{}_{}.eps".format(v, m))
        R.gPad.SetLogy(True)
        c.Print("RSGvs2HDM_{}_{}_log.eps".format(v, m))        

    return 

if __name__ == "__main__":
    R.gROOT.SetBatch(True)
    base = "inputs/HHCombClosure/13TeV_TauLH_CUT_2tag2pjet_0ptv_BDTVarsPreselection_{}.root"

    for v in ("DRTauTau", "dRbb", "mbb", "METCent", "Mhh", "mMMC", "pTtautau", "pTB1", "pTB2", "pTBB"):
        f = R.TFile.Open(base.format(v))
        plotRSGvs2HDM(f, v, [260, 275, 300, 325, 350, 400])
        f.Close()
