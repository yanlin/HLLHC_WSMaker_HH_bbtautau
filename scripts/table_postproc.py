def parse(name, region = None, keep = [], sig_name = "Expected Signal", type="postfit"):
    with open("tables/" + prefix + name + "/" + type + "/" + ("Yields_Prefit" if type == "prefit" else "Yields_GlobalFit_conditionnal_mu0") +  ".tex") as f:
        samples = []

        inregion = False

        for l in f:
            if "Region" in l and region:
                inregion = region in l

            if not inregion:
                continue

            found = [l.startswith(t) for t in keep]
            if any(found):
                i = found.index(True)
                if keep[i] in rename:
                    samples.append(l.replace(keep[i], rename[keep[i]]))
                else:
                    samples.append(l)

                if keep[i] in ["Bkg", "data"] and not "hline" in samples[-1]:
                    samples.append("\\hline \n")

            if (l.startswith("SignalExpected")):
                samples.append(l.replace("SignalExpected", sig_name))

        samples_sort = []
        samples_sort += samples

        return samples_sort

def write_table(name, samples, type="postfit"):
    if type == "prefit":
        name = name.replace(".tex", ".prefit.tex")
    with open(name, "w") as f:

        #f.write("\\hline\n")
        f.write("Sample & " + ("Post-fit" if type == "postfit" else "Pre-fit") + " yield \\\\ \n")
        f.write("\\hline\n")
        
        for s in samples:
            f.write(s)

        f.write("\\hline\n")

    return

if __name__ == "__main__":
    
    prefix = "HH231217.271217_HH_13TeV_271217_Systs_lephad_" # "HH240517.240517_HH_13TeV_240517_Systs_lephad_"            
    rename = {"Ztthf" : r"$Z \to \tau\tau + (cc, bc, bb)$",
              "Zhf" : r"$Z \to \ell\ell + (cc, bc, bb)$",
              "VH" : r"SM $VH \to \tau\tau bb, bb\tau\tau$",
              "Bkg": "Total Background",
              "data" : "Data"}

    t = "postfit"

    bkgs = ["Top quark", "Fake", "Other", "Ztthf", "Bkg", "data", "VH"]
    l = parse("RSGc1_BDT_300", region = "L1", keep = bkgs, sig_name = r"$G (300, c=1) \to bb\tau\tau$", type=t)
    l += parse("RSGc1_BDT_500", region = "L1", keep = [], sig_name = r"$G (500, c=1) \to bb\tau\tau$", type=t)
    l += parse("RSGc1_BDT_1000", region = "L1", keep = [], sig_name = r"$G (1000, c=1) \to bb\tau\tau$", type=t)

    l += parse("RSGc2_BDT_300", region = "L1", keep = [], sig_name = r"$G (300, c=2) \to bb\tau\tau$", type=t)
    l += parse("RSGc2_BDT_500", region = "L1", keep = [], sig_name = r"$G (500, c=2) \to bb\tau\tau$", type=t)
    l += parse("RSGc2_BDT_1000", region = "L1", keep = [], sig_name = r"$G (1000, c=2) \to bb\tau\tau$", type=t)    
    
    l += parse("2HDM_BDT_300", region = "L1", keep = [], sig_name = r"$H (300) \to bb\tau\tau$", type=t)
    l += parse("2HDM_BDT_500", region = "L1", keep = [], sig_name = r"$H (500) \to bb\tau\tau$", type=t)
    l += parse("2HDM_BDT_1000", region = "L1", keep = [], sig_name = r"$H (1000) \to bb\tau\tau$", type=t)    

    l += parse("SM_BDT_0", region = "L1", keep = [], sig_name = r"Non-res. $hh$", type=t)
    l += parse("SMRW_BDT_0", region = "L1", keep = [], sig_name = r"Non-res. $hh (RW)$", type=t)            
    
    write_table("bbtautau_lephad.tex", l, type=t)
    
    bkgs = ["Zhf", "Top quark", "Other", "Bkg", "data"]
    l = parse("RSGc1_BDT_300", region = "L2", keep = bkgs, type=t)
    l2 = []

    for s in l:
        if "Zhf" in s or rename["Zhf"] in s:
            l2.append(s)
            l.remove(s)

    l2 += l

    write_table("bbtautau_twolepcr.tex", l2, type=t)

    print "Don't forget to divide SM value by 1000!!!"
