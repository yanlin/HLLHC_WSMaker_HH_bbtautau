import ROOT as R
from array import array

def limits(name, masses, brs, idx = 1):

    h2d = R.TH2F("2D", "; Mass [GeV]; BR(LQ_{up} #rightarrow b#tau)", 8, 350, 1150, 10, 0.05, 1.05)

    for mass in masses:
        for br in brs:
            f = R.TFile.Open("root-files/" + name.format(int(br*10), mass) + "/" + str(mass) + ".root")
            h = f.Get("limit").Clone()
            l = h.GetBinContent(idx)

            if mass in [900, 1000, 1100] and br in [0.1, 0.2]:
                l *= 1000;

            print mass, br, l 
            h2d.Fill(mass, br, l)
            f.Close()
    return h2d

def contours(name, masses, brs, idx)

if __name__ == "__main__":
    R.gROOT.SetBatch(True)
    R.gStyle.SetOptStat(0)

    masses = [400, 500, 600, 700, 800, 900, 1000, 1100]
    brs = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    levels = array('d', [0, 1])

    c = R.TCanvas()
    h = limits("LQ280318.030418b_HH_13TeV_030418b_Systs_lephad_LQ_BDT_BR{:02d}_{}_obs", masses, brs)
    h.SetContour(2, levels)
    h.Draw("CONT Z LIST")    
    c.Update()  # Needed to save contours
    c.Print("test.eps")

    graphs = []
    cont = R.gROOT.GetListOfSpecials().FindObject("contours")
    for c in list(cont):
        for g in list(c):
            if g: graphs.append(g)

    c2 = R.TCanvas()
    a = h.Clone("axis")
    a.Reset()
    a.Draw()
    graphs[0].Draw("c")
    c2.Print("test2.eps")
