ne  =  0.4257148206233978
no  =  0.37571272253990173
np1 = ne + 0.11896371841430664
nm1 = ne - 0.16675704717636108

de  = 0.43804195523262024
do  = 0.4085620045661926
dp1 = de + 0.12240847200155258
dm1 = de - 0.17158570885658264

ue  = 0.41826704144477844
uo  = 0.4302219748497009
up1 = ue + 0.11688247323036194
um1 = ue - 0.1638396680355072

print "{:20s} & {:10s} & {:10s} & {:10s} & {:10s} \\\\".format("Version", "Obs", "-1 sig", "exp", "+1 sig")
print "{:20s} & {:10.2f} & {:10.2f} & {:10.2f} & {:10.2f} \\\\".format("Nominal 0.20 binning", no, nm1, ne, np1)
print "{:20s} & {:10.2f} & {:10.2f} & {:10.2f} & {:10.2f} \\\\".format("Down    0.19 binning", do, dm1, de, dp1)
print "{:20s} & {:10.2f} & {:10.2f} & {:10.2f} & {:10.2f} \\\\".format("Up      0.21 binning", uo, um1, ue, up1)
