import ROOT as R
from array import array

keep = []

idx_to_name = {
    1 : "obs",
    2 : "exp",
    4 : "expp1",
    5 : "expm1",
    }

def limits(name, masses, brs, idx = 1, scale = True):
    global down

    h2d = R.TGraph2D()
    h2d.SetName("2D%s" % idx)
    if down:
        h2d.SetTitle("; m_{LQ} [GeV]; BR(LQ_{down} #rightarrow t#tau)")
    else:
        h2d.SetTitle("; m_{LQ} [GeV]; BR(LQ_{up} #rightarrow b#tau)")

    for mass in masses:        

        if scale:
            f = R.TFile.Open("root-files/" + name.format(mass) + "/" + str(mass) + ".root")

        for br in brs:
            if br == 0:
                h2d.SetPoint(h2d.GetN(), mass, br, 999)
                continue
            
            if not scale:
                f = R.TFile.Open("root-files/" + name.format(int(br*10), mass) + "/" + str(mass) + ".root")

            h = f.Get("limit").Clone()
            l = h.GetBinContent(idx)

            if scale:
                l *= 1/(br**2)

            if not down and ((not scale and mass in [900, 1000, 1100] and br in [0.1, 0.2]) or (scale and mass in [1300,1400,1500])):
                l *= 1000; # Scale for extra 1000 forcing fit to converge

            h2d.SetPoint(h2d.GetN(), mass, br, l)
            if not scale:
                f.Close()

        if scale:
            f.Close()

    keep.append(h2d)
    return h2d.GetHistogram()

def contours(name, masses, brs, idx = 1, scale = True):
    graphs = []
    levels = array('d', [0, 1])

    c = R.TCanvas()
    h = limits(name, masses, brs, idx, scale)
    h.SetContour(2, levels)
    #h.GetYaxis().SetRangeUser(0.0001,1)
    h.Draw("CONT Z LIST")
    
    c.Update()  # Needed to save contours
    cont = R.gROOT.GetListOfSpecials().FindObject("contours")

    for c in list(cont):
        for g in list(c):
            if g:
                g.SetName("Contour_" + idx_to_name[idx])
                graphs.append(g.Clone())

    return graphs, h

if __name__ == "__main__":
    R.gROOT.SetBatch(True)
    R.gStyle.SetOptStat(0)
    R.gStyle.SetOptTitle(0)

    down = True
    masses = [400, 500, 600, 700, 800, 900, 1000, 1100]
    if down:
        brs = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
        name = "LQ280318.050418_HH_13TeV_050418_Systs_lephad_LQDown{:02d}_BDT_{}_obs"     # reweighting lephad
        name = "LQCombBeta05.090418b_HH_13TeV_090418b_Systs_tautau_LQDown{:02d}_BDT_{}_obs" # reweighting comb
    else:
        brs = [i/100. for i in range(1, 101)]
        # name = "LQ280318.030418b_HH_13TeV_030418b_Systs_lephad_LQ_BDT_BR{:02d}_{}_obs"  # scale in WS lephad
        name = "LQ280318.290318_HH_13TeV_290318_Systs_lephad_LQ_BDT_{0}_obs"              # scale mu lephad
        # name = "LQComb070618.090318_HH_13TeV_090318_StatOnly_tautau_LQ_BDT_{0}_obs"     # scale mu comb

    go, h = contours(name, masses, brs, 1, scale = not down)
    ge, _ = contours(name, masses, brs, 2, scale = not down)
    geu, _ = contours(name, masses, brs, 4, scale = not down)
    ged, _ = contours(name, masses, brs, 5, scale = not down)    
    
    c2 = R.TCanvas()
    a = h.Clone("axis")
    a.Reset()
    #a.GetYaxis().SetRangeUser(0.0001,1)
    a.Draw()

    ge[0].SetLineColor(1)
    ge[0].Draw("c")
    
    go[0].SetLineColor(2)
    go[0].Draw("c")

    geu[0].SetLineColor(1)
    geu[0].SetLineStyle(2)
    geu[0].Draw("c")

    ged[0].SetLineColor(1)
    ged[0].SetLineStyle(2)
    ged[0].Draw("c")

    leg = R.TLegend(0.58,0.15,0.89,0.4)    
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.AddEntry(go[0],"Observed Limit","l")
    #leg.AddEntry(,"Observed Limit #pm 1#sigma","l")
    leg.AddEntry(ge[0],"Expected Limit","l")
    leg.AddEntry(geu[0],"Expected Limit #pm 1#sigma","l")
    leg.Draw()

    if down:        
        c2.Print("LQbtaubtauDown.tautau.eps")
        c2.Print("LQbtaubtauDown.tautau.root")
    else:
        c2.Print("LQbtaubtauUp.lephad.eps")
        c2.Print("LQbtaubtauUp.lephad.root")

    #f = R.TFile.Open("LQbtaubtau.lephad.root")
    #print list(f.Get("c1").GetListOfPrimitives())
