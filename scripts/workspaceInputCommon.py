# -----------------------------------------------------------------------------
# global variables
# -----------------------------------------------------------------------------
RMPROC = "remove_proc"
RMSYST = "remove_syst"
RMHIST = "remove_hist"
MVSYST = "rename_syst"

# -----------------------------------------------------------------------------
# global settings
# -----------------------------------------------------------------------------
signals_hadhad = {"SMBDT", 
    "PNN251", "PNN260", "PNN280", "PNN300", "PNN325", "PNN350", "PNN375", 
    "PNN400", "PNN450", "PNN500", "PNN550", "PNN600", "PNN700", "PNN800", 
    "PNN900", "PNN1000", "PNN1100", "PNN1200", "PNN1400", "PNN1600"}

signals_lephad = {"SM_NN", 
    "PNN_251", "PNN_260", "PNN_280", "PNN_300", "PNN_325", "PNN_350", "PNN_375", 
    "PNN_400", "PNN_450", "PNN_500", "PNN_550", "PNN_600", "PNN_700", "PNN_800", 
    "PNN_900", "PNN_1000", "PNN_1100", "PNN_1200", "PNN_1400", "PNN_1600"}

# recommandation: first remove, then rename

def commands_hadhad(obj, **kwargs):
    remove_HadHad_TTBAR_old_param(obj)
    remove_HadHad_TTBAR_other_signals(obj, kwargs["others"])
    remove_HadHad_TTBAR_ISR_down(obj, kwargs["signal"])
    remove_HadHad_Stop(obj)
    remove_HadHad_Zjets(obj)
    remove_HadHad_Zjets_Scale_other_signals(obj, kwargs["others"])
    rename_HadHad_TTBAR_mva_param(obj, kwargs["signal"])
    rename_HadHad_TTBAR(obj)
    rename_HadHad_Stop(obj)
    rename_HadHad_Zjets(obj)
    rename_HadHad_Zjets_Scale_mva_param(obj, kwargs["signal"])
    rename_HadHad_VBFSMHH(obj)

def commands_lephad(obj, **kwargs):
    remove_LepHad_TTBAR(obj)
    remove_LepHad_TTBAR_other_signals(obj, kwargs["others"])
    remove_LepHad_Stop(obj)
    remove_LepHad_Zjets(obj)
    rename_LepHad_TTBAR_mva_param(obj, kwargs["signal"])
    rename_LepHad_Stop(obj)
    rename_LepHad_Zjets(obj)

def commands_zcr(obj, **kwargs):
    pass

# -----------------------------------------------------------------------------
# implement your renaming and removing here
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# HadHad
# -----------------------------------------------------------------------------
def remove_HadHad_TTBAR_old_param(obj):
    obj.addCommand(RMSYST, "TTBAR_ACC_PS__1up") \
       .addCommand(RMSYST, "TTBAR_ACC_PS__1down") \
       .addCommand(RMSYST, "TTBAR_ACC_GEN__1up") \
       .addCommand(RMSYST, "TTBAR_ACC_GEN__1down") \
       .addCommand(RMSYST, "TTBAR_ACC_ISR_PARAM__1up") \
       .addCommand(RMSYST, "TTBAR_ACC_ISR_PARAM__1down") \
       .addCommand(RMSYST, "TTBAR_ACC_FSR__1up") \
       .addCommand(RMSYST, "TTBAR_ACC_FSR__1down") \
       .addCommand(RMSYST, "TTBAR_ACC_MUF__1up") \
       .addCommand(RMSYST, "TTBAR_ACC_MUF__1down") \
       .addCommand(RMSYST, "TTBAR_ACC_MUR__1up") \
       .addCommand(RMSYST, "TTBAR_ACC_MUR__1down") \
       .addCommand(RMSYST, "TTBAR_ACC_VAR3C__1up") \
       .addCommand(RMSYST, "TTBAR_ACC_VAR3C__1down")

def remove_HadHad_TTBAR_other_signals(obj, others):
    [obj.addCommand(RMSYST, "TTBAR_ACC_PS_MVA_{}__1up".format(syst)) for syst in others]
    [obj.addCommand(RMSYST, "TTBAR_ACC_ME_MVA_{}__1up".format(syst)) for syst in others]
    [obj.addCommand(RMSYST, "TTBAR_ACC_ISR_MVA_{}__1up".format(syst)) for syst in others]
    [obj.addCommand(RMSYST, "TTBAR_ACC_ISR_MVA_{}__1down".format(syst)) for syst in others]

def remove_HadHad_TTBAR_ISR_down(obj, signal):
    obj.addCommand(RMSYST, "TTBAR_ACC_ISR_MVA_{}__1down".format(signal))

def remove_HadHad_Stop(obj):
    obj.addCommand(RMSYST, "SingleTop_ISR__1up")
    obj.addCommand(RMSYST, "SingleTop_ISR__1down")
    obj.addCommand(RMSYST, "SingleTop_FSR__1up")
    obj.addCommand(RMSYST, "SingleTop_FSR__1down")

def remove_HadHad_Zjets(obj):
    """
    We still use the old parametrisation.
    """
    obj.addCommand(RMSYST, "ZJETS_ACC_GENERATOR_MBB_TEST__1up") \
       .addCommand(RMSYST, "ZJETS_ACC_GENERATOR_MBB_TEST__1down")

def remove_HadHad_Zjets_Scale_other_signals(obj, others):
    [obj.addCommand(RMSYST, "ZJETS_ACC_SCALE_MVA_{}__1up".format(syst)) for syst in others]
    [obj.addCommand(RMSYST, "ZJETS_ACC_SCALE_MVA_{}__1down".format(syst)) for syst in others]

def rename_HadHad_TTBAR_mva_param(obj, signal):
    """
    ISR down is removed, take mirror of up later
    """
    obj.addCommand(MVSYST, "TTBAR_ACC_PS_MVA_{}__1up".format(signal), "THEO_ACC_TTBAR_PS_HadHad__1up") \
       .addCommand(MVSYST, "TTBAR_ACC_ME_MVA_{}__1up".format(signal), "THEO_ACC_TTBAR_ME__1up") \
       .addCommand(MVSYST, "TTBAR_ACC_ISR_MVA_{}__1up".format(signal), "THEO_ACC_TTBAR_ISR__1up")

def rename_HadHad_TTBAR(obj):
    obj.addCommand(MVSYST, "TTBAR_ACC_FSR_OUTLIERREMOVAL__1up", "THEO_ACC_TTBAR_FSR__1up") \
       .addCommand(MVSYST, "TTBAR_ACC_FSR_OUTLIERREMOVAL__1down", "THEO_ACC_TTBAR_FSR__1down")

def rename_HadHad_Stop(obj):
    obj.addCommand(MVSYST, "SingleTop_Wtchan_TopInterference__1up", "THEO_ACC_StopWt_TopInterference__1up") \
       .addCommand(MVSYST, "SingleTop_Wtchan_TopInterference__1down", "THEO_ACC_StopWt_TopInterference__1down")

def rename_HadHad_Zjets(obj):
    obj.addCommand(MVSYST, "ZJETS_ACC_GENERATOR_MBB__1up", "THEO_ACC_Zhf_GENERATOR__1up") \
       .addCommand(MVSYST, "ZJETS_ACC_GENERATOR_MBB__1down", "THEO_ACC_Zhf_GENERATOR__1down")

def rename_HadHad_Zjets_Scale_mva_param(obj, signal):
    obj.addCommand(MVSYST, "ZJETS_ACC_SCALE_MVA_{}__1up".format(signal), "THEO_ACC_Zhf_SCALE__1up") \
       .addCommand(MVSYST, "ZJETS_ACC_SCALE_MVA_{}__1down".format(signal), "THEO_ACC_Zhf_SCALE__1down")

def rename_HadHad_VBFSMHH(obj):
    obj.addCommand(MVSYST, "NONRES_VBF_MUF__1up", "THEO_ACC_SCALE_VBFSMHH__1up") \
       .addCommand(MVSYST, "NONRES_VBF_MUF__1down", "THEO_ACC_SCALE_VBFSMHH__1down")

# -----------------------------------------------------------------------------
# LepHad
# -----------------------------------------------------------------------------
def remove_LepHad_TTBAR(obj):
    """
    Do nothing, they don't exists in input
    """
    obj.addCommand(RMSYST, "TTbarClosureHerwigTrueTop__1up")
    obj.addCommand(RMSYST, "TTbarClosureHerwigTrueTop__1down")
    obj.addCommand(RMSYST, "TTbarClosureaMCTrueTop__1up")
    obj.addCommand(RMSYST, "TTbarClosureaMCTrueTop__1down")
    obj.addCommand(RMSYST, "TTbarClosureaMCpTBB__1up")
    obj.addCommand(RMSYST, "TTbarClosureaMCpTBB__1down")
    obj.addCommand(RMSYST, "TTbarClosureISRTrueTop__1up")
    obj.addCommand(RMSYST, "TTbarClosureISRTrueTop__1down")
    obj.addCommand(RMSYST, "TTBAR_ACC_GEN__1up")
    obj.addCommand(RMSYST, "TTBAR_ACC_GEN__1down")
    obj.addCommand(RMSYST, "TTBAR_ACC_PS__1up")
    obj.addCommand(RMSYST, "TTBAR_ACC_PS__1down")

def remove_LepHad_TTBAR_other_signals(obj, others):
    # don't know why lephad use SMBDT for SM_NN parametrisation
    renamed = lambda x: "SMBDT" if x == "SM_NN" else x
    [obj.addCommand(RMSYST, "TTBAR_ACC_PS_MVA_{}__1up".format(renamed(syst))) for syst in others]
    [obj.addCommand(RMSYST, "TTBAR_ACC_ME_MVA_{}__1up".format(renamed(syst))) for syst in others]

def remove_LepHad_Zjets(obj):
    """
    Do nothing, they don't exists in input
    """
    obj.addCommand(RMSYST, "Ztautauckkw__1up") \
       .addCommand(RMSYST, "Ztautauckkw__1down") \
       .addCommand(RMSYST, "Ztautauqsf__1up") \
       .addCommand(RMSYST, "Ztautauqsf__1down")

def remove_LepHad_Stop(obj):
    obj.addCommand(RMHIST, "stops_SyssingletopMUR__1up") \
       .addCommand(RMHIST, "stops_SyssingletopMUR__1down") \
       .addCommand(RMHIST, "stops_SysSingleTop_Wtchan_TopInterference__1up") \
       .addCommand(RMHIST, "stops_SysSingleTop_Wtchan_TopInterference__1down")

    obj.addCommand(RMHIST, "stopt_SyssingletopMUR__1up") \
       .addCommand(RMHIST, "stopt_SyssingletopMUR__1down") \
       .addCommand(RMHIST, "stopt_SysSingleTop_Wtchan_TopInterference__1up") \
       .addCommand(RMHIST, "stopt_SysSingleTop_Wtchan_TopInterference__1down")

def rename_LepHad_TTBAR_mva_param(obj, signal):
    # don't know why lephad use SMBDT for SM_NN parametrisation
    signal_renamed = "SMBDT" if signal == "SM_NN" else signal
    obj.addCommand(MVSYST, "TTBAR_ACC_PS_MVA_{}__1up".format(signal_renamed), "THEO_ACC_TTBAR_PS_SLT__1up")
    obj.addCommand(MVSYST, "TTBAR_ACC_ME_MVA_{}__1up".format(signal_renamed), "THEO_ACC_TTBAR_ME__1up")

def rename_LepHad_Stop(obj):
    obj.addCommand(MVSYST, "singletopMUR__1up", "THEO_ACC_StopWt_FSR__1up") \
       .addCommand(MVSYST, "singletopMUR__1down", "THEO_ACC_StopWt_FSR__1down") \
       .addCommand(MVSYST, "SingleTop_Wtchan_TopInterference__1up", "THEO_ACC_StopWt_TopInterference__1up") \
       .addCommand(MVSYST, "SingleTop_Wtchan_TopInterference__1down", "THEO_ACC_StopWt_TopInterference__1down")

def rename_LepHad_Zjets(obj):
    obj.addCommand(MVSYST, "ZtautauClosurepTBB__1up", "THEO_ACC_Zhf_SCALE__1up") \
       .addCommand(MVSYST, "ZtautauClosurepTBB__1down", "THEO_ACC_Zhf_SCALE__1down")

# -----------------------------------------------------------------------------
# ZCR
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# helper functions
# -----------------------------------------------------------------------------
def getSignal(string, signals):
    for signal in signals:
        if signal in string:
            return signal

    return None