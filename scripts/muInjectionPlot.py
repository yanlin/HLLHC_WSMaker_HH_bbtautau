import ROOT as R
from array import array
import re

pat = re.compile(".*Inj(\d+)_.*")

def plot(name, masses, inmu = 1.910e-03):

    fname = "fccs/FitCrossChecks_" + name + "_combined/FitCrossChecks.root"
    inmass = pat.match(name).groups()[0]

    vals = array("f", [])
    errs = array("f", [])
    mass = array("f", masses)
    zero = array("f", [0]*len(masses))        

    for m in masses:
        print fname
        f = R.TFile.Open(fname.format(m))
        fitResult = f.Get("PlotsAfterFitToAsimov/unconditionnal/fitResult")
        muhat =  fitResult.floatParsFinal().find("SigXsecOverSM")
        vals.append(muhat.getVal())
        errs.append(muhat.getError())
        #print m, muhat.getVal(), muhat.getError()
        f.Close()

    c = R.TCanvas()
    g = R.TGraphErrors(len(mass), mass, vals, zero, errs)

    g.GetXaxis().SetLimits(200, 1060)
    #g.GetYaxis().SetRangeUser(-2e-3, 2e-3)

    g.Draw("ap*")
    
    l = R.TLine(200, 0, 1000, 0)
    l.SetLineColor(1)
    l.SetLineStyle(2)
    l.Draw()

    l2 = R.TLine(200, inmu, 1000, inmu)
    l2.SetLineColor(2)
    l2.SetLineStyle(2)
    l2.Draw()

    print mass
    print vals
    print errs

    c.Print("mu_{0}.eps".format(inmass))

if __name__ == "__main__":

    R.gROOT.SetBatch(True)
    masses = [260, 300, 400, 500, 600, 700, 800, 900, 1000]
    #plot("HH220117_BDT12.310117Inj400_HH_13TeV_310117Inj400_Systs_lephad_2HDM_{0}", masses)

    #plot("HH250117Sys.310117Inj400_HH_13TeV_310117Inj400_Systs_lephad_2HDM_{0}", masses)

    plot("HH250117Sys.310117Inj700_HH_13TeV_310117Inj700_Systs_lephad_2HDM_{0}", masses, inmu = 1)    
