import ROOT as R
import AtlasStyle

from glob import glob
from array import array
from commands import getstatusoutput

nyield_lh = "/afs/cern.ch/user/g/gwilliam/public/yield_lh.txt"
nyield_hh = "/afs/cern.ch/user/g/gwilliam/public/yield_hh.txt"
xs = "/afs/cern.ch/user/g/gwilliam/public/XSections_13TeV.txt"

lhfact = 0.03329
hhfact = 0.03045

dosmooth = True

def ATLASLabel(t, suffix = "Simulation", pos = [220,0.098]):
    x = pos[0]
    y = pos[1]
        
    y2 = y - 0.01
    y3 = y - 0.02    
    delx = 200
    
    ts = 0.075
    
    l = R.TLatex()
    l.SetTextFont(72)    
    l.SetTextSize(ts)    
    l.DrawLatex(x, y, "ATLAS")
    p = R.TLatex()
    p.SetTextFont(42)
    p.SetTextSize(ts)
    p.DrawLatex(x+delx, y, suffix)

    ts2 = 0.05

    e = R.TLatex()
    e.SetTextFont(42)
    e.SetTextSize(ts2)
    e.DrawLatex(x, y2, "13 TeV, 36.1 fb^{-1}")

    if t == "c20":
        model = "Bulk RS k/#bar{M}_{Pl}=2"
    elif t == "c10":
        model = "Bulk RS k/#bar{M}_{Pl}=1"
    else:
        model = "Narrow Scalar"
        
    m = R.TLatex()
    m.SetTextFont(42)
    m.SetTextSize(ts2)
    m.DrawLatex(x, y3, model)


    return l,p


def ninitial(name, chan):

    res = {}

    filt = {}
    
    
    with open(nyield_lh if chan == "TauLH" else nyield_hh) as f:
        for l in f:
            tok = l.split()

            if not name in tok[-1]: continue
            ds = '_'.join(tok[-1].split("_")[1:])

            try:
                m = int(tok[-1].split("_")[-1].replace("M", ""))
            except ValueError:
                m = int(tok[-1].split("_")[-3].replace("X", "").replace("tohh", ""))

            n = float(tok[1])

            e, o = getstatusoutput("grep {} {}".format(ds, xs))
            if e:
                print "Can't find filter efff"
                print e
                print ds
                
            efilter = float(o.split()[3])

            # take out filters to get back to original number of events and correct for other decay modes generated (include with extra 0.414)
            # Add back in to intial number the lh/hh so get efficiency relative to that

            res[m] = n / efilter * (lhfact if chan == "TauLH" else hhfact)

#             if chan == "TauLH" and "c20" in name:
#                 if 260 <= m <= 400:
#                     filt[m] = efilter
                

    return res

def nfinal(pat, masses, chan, t):

    res = {}
    for m in masses:
        if t == 'H':
            n = "Hhhbbtautau{}"
        else:
            n = "Ghhbbtautau{}" + t

        h = None; num = 0 
        for fn in glob(pat.format(m)):
            if not chan in fn: continue

            f = R.TFile.Open(fn)
            htmp = f.Get(n.format(m))
            if not htmp: continue

            num += htmp.GetEntries()

            try:
                h.Add(htmp)
            except AttributeError:
                h = htmp.Clone()

        res[m] = num

    return res

def divide(initial, final):
    x = array('f', [])
    y = array('f', [])
    
    for m, nf in sorted(final.iteritems()):
        try:
            print "{:5d}{:5.3f}".format(m, nf/initial[m])
            x.append(m)
            y.append(nf/initial[m])
        except KeyError:
            pass
    return x, y

def smooth(g, xh):
    f = R.TF1("Fit" + g.GetName(), "pol6", 260, 1000)
    g.Fit(f, "RN")
    yh = array("f", [f.Eval(x) for x in xh])
    g2 = R.TGraph(g.GetN(), xh, yh)

    return g2

def acc(initial_lh, final_lh, initial_hh, final_hh, t):

    #leg = R.TLegend(0.2,0.75,0.89,0.89)
    leg = R.TLegend(0.7,0.25,0.89,0.45)    
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.05)
    
    print t, "lh"
    xl, yl = divide(initial_lh, final_lh)
    gl = R.TGraph(len(xl), xl, yl)
    gl.SetLineColor(2)
    gl.SetMarkerColor(2)    
    gl.SetMarkerStyle(22)
    leg.AddEntry(gl, "#tau_{lep}#tau_{had}", "lp")

    print t, "hh"
    xh, yh = divide(initial_hh, final_hh)
    eh = array('f', [y**0.5 for y in yh])
    zero = array('f', [0 for x in xh])    
    gh = R.TGraph(len(xh), xh, yh)
    #gh = R.TGraphSmooth("normal")
    #gh.Approx(ghin, "linear")
    
    gh.SetLineColor(4)
    gh.SetMarkerColor(4)    
    gh.SetMarkerStyle(10)
    leg.AddEntry(gh, "#tau_{had}#tau_{had}", "lp")
    
    gh.SetMinimum(0.)
    gh.SetMaximum(0.11)
    gh.SetTitle(";m({}) [GeV]; Acceptance #times Efficiency".format("X" if t == "H" else "G_{kk}"))
    
    c = R.TCanvas()

    if dosmooth:
        ghs = smooth(gh, xh)    
        ghs.SetLineColor(4)
        ghs.SetMarkerColor(4)    
        ghs.SetMarkerStyle(10)
        ghs.Draw("apc")
        
        ghs.SetMinimum(0.)
        ghs.SetMaximum(0.11)
        ghs.SetTitle(";m({}) [GeV]; Acceptance #times Efficiency".format("X" if t == "H" else "G_{kk}"))

        gls = smooth(gl, xl)    
        gls.SetLineColor(2)
        gls.SetMarkerColor(2)    
        gls.SetMarkerStyle(22)
        gls.Draw("pc") 
    else:
        gh.Draw("apc")    
        gl.Draw("pc")

    # Digitised values
    print "*"*50
    for i in xrange(gls.GetN()):
        lx = R.Double()
        ly = R.Double()
        hx = R.Double()
        hy = R.Double()        

        gls.GetPoint(i, lx, ly)
        ghs.GetPoint(i, hx, hy)        
        print "{:5.0f} & {:5.5f} & {:5.5f} \\\\".format(lx, ly, hy)
    print "*"*50

    leg.Draw()
    ATLASLabel(t)    
    
    c.Print("Acc_{}.eps".format(t))
    c.Print("Acc_{}.pdf".format(t))    

    return

if __name__ == "__main__":

    R.gROOT.SetBatch(True)

    masses = [260, 275, 300, 325, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000]
    masses2 = [260, 300, 350, 400, 500, 600, 700, 800, 900, 1000]

    c1 = "/eos/user/g/gwilliam/HHbbtautauCombination/*ttH/13TeV_Tau*_2tag2pjet_0ptv_SR_RSGc1_BDT_{}.root"
    c2 = "/eos/user/g/gwilliam/HHbbtautauCombination/*ttH/13TeV_Tau*_2tag2pjet_0ptv_SR_RSGc2_BDT_{}.root"
    h2 = "/eos/user/g/gwilliam/HHbbtautauCombination/*ttH/13TeV_Tau*_2tag2pjet_0ptv_SR_2HDM_BDT_{}.root"        

    igc1_lh = ninitial("RS_G_hh_bbtt_lh_c10", chan = "TauLH")
    igc2_lh = ninitial("RS_G_hh_bbtt_lh_c20", chan = "TauLH")
    ih_lh   = ninitial("tohh_bbtautau", chan = "TauLH")    

    igc1_hh = ninitial("RS_G_hh_bbtt_hh_c10", chan = "TauHH")
    igc2_hh = ninitial("RS_G_hh_bbtt_hh_c20", chan = "TauHH")    
    ih_hh   = ninitial("tohh_bbtautau", chan = "TauHH")    

    gc1_lh = nfinal(c1, masses2, "TauLH", "c10")
    gc2_lh = nfinal(c2, masses, "TauLH", "c20")
    h_lh   = nfinal(h2, masses, "TauLH", "H")

    gc1_hh = nfinal(c1, masses2, "TauHH", "c10")
    gc2_hh = nfinal(c2, masses, "TauHH", "c20")
    h_hh   = nfinal(h2, masses, "TauHH", "H")

    acc(igc1_lh, gc1_lh, igc1_hh, gc1_hh, "c10")
    acc(igc2_lh, gc2_lh, igc2_hh, gc2_hh, "c20")
    acc(ih_lh, h_lh, ih_hh, h_hh, "H")    


#     f = {260: 0.022998, 275: 0.022563, 300: 0.022079, 325: 0.022092, 350: 0.022253, 400: 0.023215}
# 
#     a = {260 : 0.0427518243316,
#          275 : 0.0407340432562,
#          300 : 0.0364235066496,
#          325 : 0.0350817509162,
#          350 : 0.0363647106826,
#          400 : 0.0428283783031}
# 
#     for m in sorted(f.keys()):
#         print m, f[m]/0.022079, a[m]/0.0364235066496
