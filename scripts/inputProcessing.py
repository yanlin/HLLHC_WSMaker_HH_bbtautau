#!/usr/bin/env python

import re, os, sys, shutil, argparse
import multiprocessing, signal
from workspaceInput import *
from workspaceInputCommon import *

# -----------------------------------------------------------------------------
# main functions
# -----------------------------------------------------------------------------

def main_hadhad(obj, signal):
    if signal not in signals_hadhad:
        raise RuntimeError("signal [{}] not in signals_hadhad, check workspaceInputCommon".format(args.signal))    
    others = signals_hadhad - {signal}
    # put parameter to the commands here
    commands_hadhad(obj, others=others, signal=signal)

def main_lephad(obj, signal):
    if signal not in signals_lephad:
        raise RuntimeError("signal [{}] not in signals_lephad, check workspaceInputCommon".format(args.signal))    
    others = signals_lephad - {signal}
    commands_lephad(obj, others=others, signal=signal)

def main_zcr(obj):
    commands_zcr(obj)

def main_helper(args, file_path, p):
    file_name = os.path.basename(file_path)
    print "Channel [{}] processing [{}]".format(args.channel, file_name)
    m = p.match(file_name)
    chan, string = m.groups()

    wsi = cWorkspaceInput(file_path, os.path.join(args.output_path, file_name))
    
    if args.channel == "hadhad" and "TauHH" in chan:
        main_hadhad(wsi, getSignal(string, signals_hadhad) or args.signal)
    elif args.channel == "lephad" and "TauLH" in chan:
        main_lephad(wsi, getSignal(string, signals_lephad) or args.signal)
    elif args.channel == "zcr" and "TwoLepton" in chan:
        main_zcr(wsi)
    else:
        print "WARN file channel [{}] and run channel [{}] does not match! will skip".format(chan, args.channel)
    
    # print "====== Before Processing ======"
    
    wsi.setDebug(False).execute().save()
    
    # print "====== After Processing ======"
    # for syst_name in wsi.systematicsList():
    #     print syst_name

def initializer():
    signal.signal(signal.SIGINT, signal.SIG_IGN)

def main(args):
    if not os.path.exists(args.output_path):
        os.mkdir(args.output_path)

    p = re.compile(r"13TeV_(.*)_([0-9]tag.*)")
    inputs = args.input_pattern

    if args.signal not in signals_hadhad and args.signal not in signals_lephad:
        print("ERROR signal [{}] is invalid".format(args.signal))
        return

    pool = multiprocessing.Pool(processes=args.ncores, initializer=initializer)

    try:
        for file_path in inputs:
            pool.apply_async(main_helper, (args, file_path, p))
    except KeyboardInterrupt:
        pool.terminate()
        pool.join()

    pool.close()
    pool.join()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # input and output
    parser.add_argument("input_pattern", nargs='*', type=str, help="input pattern")
    parser.add_argument("output_path", type=str, help="output path")

    # analysis info (SLT, LTT not splitted)
    parser.add_argument("-c", "--channel", choices=["lephad", "hadhad", "zcr"], type=str, 
        default="hadhad", help="channel to be included, choose from (lephad, hadhad, zcr)")

    parser.add_argument("-s", "--signal", type=str, 
        default="SMBDT", help="manually set a MVA score variable")

    # other
    parser.add_argument("--ncores", type=int, default=8)
    
    args = parser.parse_args()

    main(args)
