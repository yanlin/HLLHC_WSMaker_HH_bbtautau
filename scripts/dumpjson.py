import json

from limit_lq import xsect

import ROOT as R

class Dumper:
    def __init__(self):
        self.data = []
        pass

    def append(self, mass, br, ce, co, cm1, cm2, cp1, cp2, xs, le, lo, lm1, lm2, lp1, lp2):
        d = {"CLs":co,"CLsexp":ce,"clsd1s":cm1,"clsd2s":cm2,"clsu1s":cp1,"clsu2s":cp2,"covqual":3.000000e+00,"dodgycov":0.000000e+00,"excludedXsec":xs,"expectedUpperLimit":le,"expectedUpperLimitMinus1Sig":lm1,"expectedUpperLimitMinus2Sig":lm2,"expectedUpperLimitPlus1Sig":lp1,"expectedUpperLimitPlus2Sig":lp2,"fID":-1.000000e+00,"failedcov":0.000000e+00,"failedfit":0.000000e+00,"failedp0":0.000000e+00,"failedstatus":0.000000e+00,"fitstatus":0.000000e+00,"m0":mass,"m12":br,"mode":-1.000000e+00,"nexp":-1.000000e+00,"nofit":0.000000e+00,"p0":3.504227e-01,"p0d1s":-1.000000e+00,"p0d2s":-1.000000e+00,"p0exp":-1.000000e+00,"p0u1s":-1.000000e+00,"p0u2s":-1.000000e+00,"p1":3.504227e-01,"seed":0.000000e+00,"sigma0":-1.000000e+00,"sigma1":-1.000000e+00,"upperLimit":lo,"upperLimitEstimatedError":-1.000000e+00,"xsec":xs}
        self.data.append(d)
        return

    def dump(self, fname = "limits.json"):
        with open(fname, "w") as f:
            json.dump(self.data, f, sort_keys = True)
        return

    def clear(self):
        self.data = []

if __name__ == "__main__":
    d = Dumper()

    massesbr = [400, 500, 600, 700, 800, 900, 1100]
    brs = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]

    
    for b in brs:
        for m in massesbr:
            xs = xsect(scale=False, chan="lephad", decay=True, stat = False, br = None, xstype = "new", scale1000 = True if b in [0.1, 0.2] else False)
            fname = "root-files/LQ280318.310318_HH_13TeV_310318_Systs_lephad_LQUp{:02d}_BDT_{}_obs/{}.root".format(int(b*10), m, m)
            f = R.TFile.Open(fname)
            h = f.Get("limit")

            d.append(m, b, h.GetBinContent(12), h.GetBinContent(11), h.GetBinContent(15), h.GetBinContent(16), h.GetBinContent(14), h.GetBinContent(13), xs[m],
                     h.GetBinContent(2), h.GetBinContent(1), h.GetBinContent(5), h.GetBinContent(6), h.GetBinContent(4), h.GetBinContent(3))

            f.Close()

    d.dump(fname = "limits.lephad.reweight.json")
    d.clear()

    for b in brs:
        for m in massesbr:
            xs = xsect(scale=False, chan="lephad", decay=True, stat = False, br = b, xstype = "new", scale1000 = False)

            #fname = "root-files/LQ280318.310318_HH_13TeV_310318_Systs_lephad_LQ_BDT_{}_obs/{}.root".format(m, m)
            fname = "root-files/LQ280318.030418b_HH_13TeV_030418b_Systs_lephad_LQ_BDT_BR{:02d}_{}_obs/{}.root".format(int(b*10), m, m)            
            f = R.TFile.Open(fname)
            h = f.Get("limit")

            d.append(m, b, h.GetBinContent(12), h.GetBinContent(11), h.GetBinContent(15), h.GetBinContent(16), h.GetBinContent(14), h.GetBinContent(13), xs[m],
                     h.GetBinContent(2), h.GetBinContent(1), h.GetBinContent(5), h.GetBinContent(6), h.GetBinContent(4), h.GetBinContent(3))

            f.Close()

    d.dump(fname = "limits.lephad.scale.json")
