git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-physics/HDBS/DiHiggs/bbtautau/WSMaker_HH_bbtautau.git

source setup.sh

cd build

cmake ..

make -j5

cd ..

## [Prepare the inputs](doc/README.PrepareInputs.md)

## [Create the workspace and run the limits](doc/README.CreateWSRunLimits.md)

## [Signal Injection tests](doc/README.SignalInjection.md)

